import * as React from "react";
import { List, Datagrid, TextField, ArrayField, SingleFieldList, ChipField, FunctionField } from 'react-admin';
import Moment from 'react-moment';

export const Users = (props) => {
  return (
    <List {...props} title="List of posts">
      <Datagrid>
        <TextField source="id" />
        <TextField source="name" />
        <TextField source="username" component="div" />
        <FunctionField label="Age" render={r => <Moment date={r.dob} durationFromNow />} />
        <ArrayField source="tags">
          {/* <Datagrid>            <TextField source="tag" />      </Datagrid> */}
          <SingleFieldList>
            <ChipField source="tag" />
          </SingleFieldList>
        </ArrayField>
      </Datagrid>
    </List>

  )
};
