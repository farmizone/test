import * as React from "react";
import { Admin, Resource } from 'react-admin';
import jsonServerProvider from 'ra-data-json-server';

import { PostList } from './posts';
import { Users } from './users';

const App = () => (
  <Admin dataProvider={jsonServerProvider('http://localhost:8080')}>
    <Resource name="posts" list={PostList} />
    <Resource name="users" list={Users} />
  </Admin>
);

export default App;