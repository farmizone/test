import * as React from "react";
import { List, Datagrid, TextField } from 'react-admin';

export const PostList = (props) => (
  <List {...props} title="List of posts">
    <Datagrid>
      <TextField source="id" />
      <TextField source="title" />
      <TextField source="body" component="div" />
    </Datagrid>
  </List>
);
