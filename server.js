const express = require("express");
const app = express();
var cors = require('cors')
const posts = require('./posts.json')
const users = require('./users.json')

app.use(cors({
  exposedHeaders: 'X-Total-Count'
}))
app.get("/", (req, res) => {
  res.send("Hello World!");
});
app.get("/posts", (req, res) => {
  res
    .setHeader('X-Total-Count', 10)
    .send(posts);
});
app.get("/users", (req, res) => {
  res.setHeader('X-Total-Count', 10).send(users);
});

const PORT = 8080;

app.listen(PORT, console.log(`Server started on port ${PORT}`));